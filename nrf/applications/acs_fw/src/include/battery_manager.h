#pragma once

#include <zephyr.h>
#include <zephyr/types.h>

typedef struct battery_readings_t
{
	int32_t m_battery_readings;
} battery_readings_t;

/** Enable or disable measurement of the battery voltage.
 *
 * @param enable true to enable, false to disable
 *
 * @return zero on success, or a negative error code.
 */
int battery_measure_enable(bool enable);

/** Measure the battery voltage.
 *
 * @return the battery voltage in millivolts, or a negative error
 * code.
 */
battery_readings_t battery_sample(void);
