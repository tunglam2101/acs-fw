#include "bt_manager.h"
#include "utils.h"

#include <stdlib.h>

#define LOG_MODULE_NAME bt_manager
LOG_MODULE_REGISTER(LOG_MODULE_NAME);

/* TODO: Using random value for testing for now. Can generate using
 * https://www.uuidgenerator.net/
 *
 * Also add more characteristics if needed
 */
#define BT_SERVICE_UUID	   0x181a
#define BT_TEMP_UUID	   0x2a6e
#define BT_HUMID_UUID	   0x2a6f
#define BT_CO2_UUID		   0x2bd5
#define BT_BATTERY_UUID	   0x2a19
#define BT_DEV_STATUS_UUID 0x2bbb

#define DEFAULT_CHRC_PROPS BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY
#ifdef CONFIG_SECURITY_ENABLED
#define DEFAULT_CHRC_PERMS BT_GATT_PERM_READ_ENCRYPT
#define DEFAULT_CCC_PERMS                                                     \
	BT_GATT_PERM_READ_ENCRYPT | BT_GATT_PERM_WRITE_ENCRYPT
#else
#define DEFAULT_CHRC_PERMS BT_GATT_PERM_READ
#define DEFAULT_CCC_PERMS  BT_GATT_PERM_READ | BT_GATT_PERM_WRITE
#endif /* CONFIG_SECURITY_ENABLED */

static const uint8_t k_num_attrs_per_chrc = 3;

/* Array of data structures used in advertising */
static struct bt_data k_adv_data_structs[]
	= {
		  BT_DATA_BYTES(BT_DATA_FLAGS,
			  (BT_LE_AD_GENERAL
				  | BT_LE_AD_NO_BREDR)), /* Specify BLE advertising flags =
											discoverable, BR/EDR not supported
											(BLE only) */
		  BT_DATA_BYTES(BT_DATA_UUID16_ALL, BT_UUID_16_ENCODE(BT_SERVICE_UUID) /* A 16 bit service UUID for the our custom service follows */),
	  };

/* UUIDs */
static struct bt_uuid_16 service_uuid = BT_UUID_INIT_16(BT_SERVICE_UUID);

/* Stored data */
static gatt_chrc_t temp_chrc
	= { .m_data = 0, .m_uuid = BT_UUID_INIT_16(BT_TEMP_UUID) };
static gatt_chrc_t humid_chrc
	= { .m_data = 0, .m_uuid = BT_UUID_INIT_16(BT_HUMID_UUID) };
static gatt_chrc_t co2_chrc
	= { .m_data = 0, .m_uuid = BT_UUID_INIT_16(BT_CO2_UUID) };
static gatt_chrc_t battery_chrc
	= { .m_data = 0, .m_uuid = BT_UUID_INIT_16(BT_BATTERY_UUID) };
static gatt_chrc_t dev_status_chrc
	= { .m_data = 0, .m_uuid = BT_UUID_INIT_16(BT_DEV_STATUS_UUID) };

/* Connections */
static struct bt_conn* p_active_conn = NULL;

/* CCCD */
static bool b_cccd_enabled = false;

static bool b_initialized = false;

/* GATT service definition*/
BT_GATT_SERVICE_DEFINE(acs_svc,
	BT_GATT_PRIMARY_SERVICE(&service_uuid),
	BT_GATT_CHARACTERISTIC((const struct bt_uuid*)&temp_chrc.m_uuid,
		DEFAULT_CHRC_PROPS,
		DEFAULT_CHRC_PERMS,
		read_chrc,
		NULL /* __write__ */,
		&temp_chrc.m_data),
	BT_GATT_CCC(cccd_cfg_changed, DEFAULT_CCC_PERMS),
	BT_GATT_CHARACTERISTIC((const struct bt_uuid*)&humid_chrc.m_uuid,
		DEFAULT_CHRC_PROPS,
		DEFAULT_CHRC_PERMS,
		read_chrc,
		NULL /* __write__ */,
		&humid_chrc.m_data),
	BT_GATT_CCC(cccd_cfg_changed, DEFAULT_CCC_PERMS),
	BT_GATT_CHARACTERISTIC((const struct bt_uuid*)&co2_chrc.m_uuid,
		DEFAULT_CHRC_PROPS,
		DEFAULT_CHRC_PERMS,
		read_chrc,
		NULL /* __write__ */,
		&co2_chrc.m_data),
	BT_GATT_CCC(cccd_cfg_changed, DEFAULT_CCC_PERMS),
	BT_GATT_CHARACTERISTIC((const struct bt_uuid*)&battery_chrc.m_uuid,
		DEFAULT_CHRC_PROPS,
		DEFAULT_CHRC_PERMS,
		read_chrc,
		NULL /* __write__ */,
		&battery_chrc.m_data),
	BT_GATT_CCC(cccd_cfg_changed, DEFAULT_CCC_PERMS),
	BT_GATT_CHARACTERISTIC((const struct bt_uuid*)&dev_status_chrc.m_uuid,
		DEFAULT_CHRC_PROPS,
		DEFAULT_CHRC_PERMS,
		read_chrc,
		NULL /* __write__ */,
		&dev_status_chrc.m_data),
	BT_GATT_CCC(cccd_cfg_changed, DEFAULT_CCC_PERMS),
	/* TODO: Expand characteristics if needed */);

/* Callbacks */
static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
#ifdef CONFIG_SECURITY_ENABLED
	.security_changed = security_changed,
#endif /* CONFIG_SECURITY_ENABLED */
};

#ifdef CONFIG_SECURITY_ENABLED
static struct bt_conn_auth_cb conn_auth_callbacks = {
	.passkey_display = auth_passkey_display,
	.passkey_confirm = auth_passkey_confirm,
	.cancel = auth_cancel,
	.pairing_confirm = pairing_confirm,
	.pairing_complete = pairing_complete,
	.pairing_failed = pairing_failed,
};
#endif /* CONFIG_SECURITY_ENABLED */

void
cccd_cfg_changed(const struct bt_gatt_attr* attr, uint16_t value)
{
	ARG_UNUSED(attr);
	b_cccd_enabled = (value == BT_GATT_CCC_NOTIFY);
}

bool
is_connection_established()
{
	return p_active_conn != NULL;
}

ssize_t
read_chrc(struct bt_conn* conn,
	const struct bt_gatt_attr* attr,
	void* buf,
	uint16_t len,
	uint16_t offset)
{
	/* Could use 'const char *value =  attr->user_data' also here if there is
	the char value is being maintained with the BLE STACK */
	uint8_t off = (attr - acs_svc.attrs + 1) / k_num_attrs_per_chrc;
	const char* p_value = NULL;
	switch (off)
	{
	case TEMP_OFFSET:
		LOG_DBG("Got a temperature read (%p); current val is: %d", attr,
			temp_chrc.m_data);
		p_value = (const char*)&temp_chrc.m_data;
		break;

	case HUMID_OFFSET:
		LOG_DBG("Got a humidity read (%p); current val is: %d", attr,
			humid_chrc.m_data);
		p_value = (const char*)&humid_chrc.m_data;
		break;

	case CO2_OFFSET:
		LOG_DBG(
			"Got a CO2 read (%p); current val is: %d", attr, co2_chrc.m_data);
		p_value = (const char*)&co2_chrc.m_data;
		break;

	case BATTERY_OFFSET:
		LOG_DBG("Got a battery read (%p); current val is: %d", attr,
			battery_chrc.m_data);
		p_value = (const char*)&battery_chrc.m_data;
		break;

	case DEV_STATUS_OFFSET:
		LOG_DBG("Got a device status read (%p); current val is: %s", attr,
			log_strdup(device_status_to_string(dev_status_chrc.m_data)));
		p_value = (const char*)&dev_status_chrc.m_data;
		break;

	default:
		LOG_ERR("Unknown attribute");
		return -1;
	}
	return bt_gatt_attr_read(conn, attr, buf, len, offset, p_value,
		sizeof(k_default_chrc_data_size)); /* Pass the value back up through
											  the BLE stack */
}

void
exchange_func(struct bt_conn* conn,
	uint8_t err,
	struct bt_gatt_exchange_params* params)
{
	LOG_INF("MTU exchange %s", err == 0 ? "successful" : "failed");
	LOG_INF("Current MTU: %u", bt_gatt_get_mtu(conn));
}

void
connected(struct bt_conn* conn, uint8_t err)
{
	if (err)
	{
		LOG_ERR("Connection failed (err 0x%02x)", err);
	}
	else
	{
		char addr_str[BT_ADDR_LE_STR_LEN];
		bt_addr_le_to_str(bt_conn_get_dst(conn), addr_str, sizeof(addr_str));
		LOG_INF("Connected to: %s", log_strdup(addr_str));

		static struct bt_gatt_exchange_params exchange_params;

		exchange_params.func = exchange_func;
		err = bt_gatt_exchange_mtu(conn, &exchange_params);
		if (err)
		{
			LOG_WRN("MTU exchange failed (err %d)", err);
		}
		else
		{
			LOG_WRN("MTU exchange pending");
		}

#ifdef CONFIG_SECURITY_ENABLED
		err = bt_conn_set_security(conn, BT_SECURITY_L4);
		if (err)
		{
			LOG_WRN("Failed to set security: %d", err);
		}
#endif /* CONFIG_SECURITY_ENABLED */
		p_active_conn = bt_conn_ref(conn);
	}
}

void
disconnected(struct bt_conn* conn, uint8_t reason)
{
	LOG_INF("Disconnected (reason 0x%02x)", reason);

	if (p_active_conn)
	{
		bt_conn_unref(p_active_conn);
		p_active_conn = NULL;
	}

	b_cccd_enabled = false;
}

#ifdef CONFIG_SECURITY_ENABLED
void
auth_cancel(struct bt_conn* conn)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	LOG_INF("Pairing cancelled: %s", log_strdup(addr));
}

void
pairing_confirm(struct bt_conn* conn)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	bt_conn_auth_pairing_confirm(conn);

	LOG_INF("Pairing confirmed: %s", log_strdup(addr));
}

void
pairing_complete(struct bt_conn* conn, bool bonded)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	LOG_INF("Pairing completed: %s, bonded: %d", log_strdup(addr), bonded);
}

void
pairing_failed(struct bt_conn* conn, enum bt_security_err reason)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	LOG_WRN("Pairing failed conn: %s, reason %d", log_strdup(addr), reason);
}

void
security_changed(struct bt_conn* conn,
	bt_security_t level,
	enum bt_security_err err)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	if (!err)
	{
		LOG_INF("Security changed: %s level %u", log_strdup(addr), level);
	}
	else
	{
		LOG_WRN("Security failed: %s level %u err %d", log_strdup(addr), level,
			err);
	}
}

void
auth_passkey_display(struct bt_conn* conn, unsigned int passkey)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	LOG_INF("Passkey for %s: %06u", log_strdup(addr), passkey);
}

void
auth_passkey_confirm(struct bt_conn* conn, unsigned int passkey)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));
	bt_conn_auth_passkey_confirm(conn);

	LOG_INF("Passkey for %s: %06u", log_strdup(addr), passkey);
}
#endif /* CONFIG_SECURITY_ENABLED */

bool
bt_ready()
{
	int err = 0;

	bt_conn_cb_register(&conn_callbacks);

#ifdef CONFIG_SECURITY_ENABLED
	err = bt_conn_auth_cb_register(&conn_auth_callbacks);
	if (err)
	{
		LOG_ERR("Failed to register authorization callbacks");
		return false;
	}
#endif /* CONFIG_SECURITY_ENABLED */

	/* Start BLE advertising using the ad array defined above */
	err = bt_le_adv_start(BT_LE_ADV_CONN_NAME, k_adv_data_structs,
		ARRAY_SIZE(k_adv_data_structs), NULL, 0);
	if (err)
	{
		LOG_ERR("Advertising failed to start (err %d)", err);
		return false;
	}

	LOG_INF("Advertising successfully started");
	return true;
}

bool
init_bt_manager()
{
	int err = 0;

	err = bt_enable(NULL);
	if (err)
	{
		LOG_ERR("Bluetooth init failed (err %d)", err);
		return false;
	}

	settings_load();
	if (err)
	{
		LOG_WRN("Settings load failed (err %d)", err);
		return false;
	}

	err = !bt_ready();
	if (err)
	{
		LOG_ERR("Bluetooth ready failed");
		return false;
	}

#ifdef CONFIG_SECURITY_ENABLED
	/* FIXME: ACS-46 - Workaround for reaching the maximum number of paired
	 * devices. We'll simply reset the device and unpair everything. This
	 * workaround, however, assumes that the user will have to reset the
	 * device if a new connection on another client device is being made. In
	 * case of runtime failures and a reset occurs, the user also has to
	 * reinitialize the pairing sequence
	 */
	err = bt_unpair(BT_ID_DEFAULT, BT_ADDR_LE_ANY);
	if (err)
	{
		LOG_ERR("Bluetooth failed to clear pairings");
		return false;
	}
#endif /* CONFIG_SECURITY_ENABLED */

	LOG_INF("Bluetooth manager initialized");

	b_initialized = true;
	return true;
}

void
update_notify_bt_client(sht3xd_readings_t sht3xd_readings,
	s8lp_readings_t s8lp_readings,
	battery_readings_t battery_readings,
	device_status_t dev_status)
{
	if (!b_initialized)
	{
		return;
	}

	temp_chrc.m_data = sensor_value_to_int32(sht3xd_readings.m_temp);
	humid_chrc.m_data = sensor_value_to_int32(sht3xd_readings.m_humid);
	co2_chrc.m_data = s8lp_readings.m_co2_readings;
	battery_chrc.m_data = battery_readings.m_battery_readings;
	dev_status_chrc.m_data = dev_status;
	if (p_active_conn && b_cccd_enabled)
	{
		bt_gatt_notify(p_active_conn,
			&acs_svc.attrs[TEMP_OFFSET * k_num_attrs_per_chrc - 1],
			&temp_chrc.m_data, sizeof(k_default_chrc_data_size));
		bt_gatt_notify(p_active_conn,
			&acs_svc.attrs[HUMID_OFFSET * k_num_attrs_per_chrc - 1],
			&humid_chrc.m_data, sizeof(k_default_chrc_data_size));
		bt_gatt_notify(p_active_conn,
			&acs_svc.attrs[CO2_OFFSET * k_num_attrs_per_chrc - 1],
			&co2_chrc.m_data, sizeof(k_default_chrc_data_size));
		bt_gatt_notify(p_active_conn,
			&acs_svc.attrs[BATTERY_OFFSET * k_num_attrs_per_chrc - 1],
			&battery_chrc.m_data, sizeof(k_default_chrc_data_size));
		bt_gatt_notify(p_active_conn,
			&acs_svc.attrs[DEV_STATUS_OFFSET * k_num_attrs_per_chrc - 1],
			&dev_status_chrc.m_data, sizeof(k_default_chrc_data_size));
	}
}
