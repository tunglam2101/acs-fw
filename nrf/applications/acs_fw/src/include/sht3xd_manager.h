#pragma once

#include <drivers/sensor.h>

typedef struct sht3xd_readings_t
{
	struct sensor_value m_temp;
	struct sensor_value m_humid;
} sht3xd_readings_t;

/**
 * @brief Init sequence for a SHT3x sensor
 *
 * @return True if SHT3XD is initialized successfully, false otw
 */
bool init_sht3xd();

/**
 * @brief Get the SHT3x readings object
 *
 * @return SHT3x readings object that contains temperature and humidity
 * readings
 */
sht3xd_readings_t get_sht3xd_readings();
