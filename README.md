
# Airtame Classroom Sensor - Firmware


## Repository for the Firmware development of Airtame Classroom Sensor (ACS) Project


### Overview
The repository is based on the [nRF Connect SDK](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/introduction.html) developed by Nordic Semiconductor. It contains a set of open source projects, including:

 - [`sdk-nrf`](https://github.com/nrfconnect/sdk-nrf) - nRF devices' supported libraries, and drivers

 - [`sdk-nrfxlib`](https://github.com/nrfconnect/sdk-nrfxlib) - closed-source libraries and modules in binary format

 - [`sdk-mcuboot`](https://github.com/nrfconnect/sdk-mcuboot) - fork of the [MCUboot](https://www.mcuboot.com) project, which provides a secure bootloader application

 - [`sdk-zephyr`](https://github.com/nrfconnect/sdk-zephyr) - fork of the [Zephyr](https://zephyrproject.org/) project (Zephyr RTOS)

 - [`sdk-mbedtls`](https://github.com/ARMmbed/mbedtls) - fork of the Mbed TLS project - a C library that implements cryptographic primitives, X.509 certificate manipulation and the SSL/TLS and DTLS protocols

For keeping the repository minimal, the repository only contains a fork of `sdk-nrf`, which contains the core of the SDK (drivers, libraries, etc.) The other libraries can be fetched manually during the setup process


### Setup
**The following setup process assumes you're working on a Linux/Windows WSL machine. For Windows setup, please follow these [instructions](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.7.1/nrf/gs_assistant.html)**

The setup process includes the following steps:

 - Clone the repository: `git clone https://bitbucket.org/tunglam2101/acs-fw.git`
 - `cd acs-fw/scripts; ./setup.sh`

    - `setup.sh` simply packs these [instructions](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.7.1/nrf/gs_installing.html) from nRF Connect SDK documents, including downloading and installing the necessary tools; populating the correct env variables

    - **Notes**: from the nRF Connect SDK [documents](To%20be%20able%20to%20cross-compile%20your%20applications%20for%20Arm%20targets,%20you%20must%20install%20version%209-2019-q4-major%20of%20the%20GNU%20Arm%20Embedded%20Toolchain.):

        > To be able to cross-compile your applications for Arm targets, you must install version 9-2019-q4-major of the [GNU Arm Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)

        Please do not up/downgrade the GCC toolchain. Otherwise this will not work

- `west update; west zephyr-export`

	> The Zephyr project includes a tool called west that enables you to manage multiple repositories. When developing in the nRF Connect SDK, your application will use libraries and features from folders that are cloned from different repositories or projects. The west tool keeps control of which commits to use from the different projects. It also makes it fairly simple to add and remove modules.

And you're done! `west update` should take awhile depending on the network connection. After this, your `acs-fw` folder should look like:

    .
    ├── bootloader
    ├── mbedtls
    ├── modules
    ├── nrf
    ├── nrfxlib
    ├── scripts
    ├── test
    ├── tools
    └── zephyr


### Build & Flash
For building and flashing your application:

 - Simply `cd` into your application folder and run `west build -b <build target> -- -DBUILD=<build type (i.e. debug/release)>` for building. More info on the supported boards can be found [here](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.7.1/nrf/app_boards.html)

 - Connect your board to your dev PC and power it up

 - Run `west flash [--erase]`
    - Use `--erase` when you want to clears the full flash memory before programming. Though this it ***recommended***

For example, building and flashing the ACS FW (debug build) onto a nRF52 DK (nRF52810):

	cd nrf/applications/acs_fw
	west build -b nrf52dk_nrf52810 -- -DBUILD=debug
	west flash --erase`


### Serial logging:
To see the UART output, connect to the development kit with a terminal emulator, for example, Minicom, PuTTY. For my setup, I'll be using Minicom:

 - Install Minicom: `sudo apt install minicom`

 - You'll then need the serial device's path. To obtain this, connect your board to your dev PC, power it up and run `dmesg`; look for your board's information. For example:

    ```
    [ 5431.295971] usb 1-11: Product: J-Link
    [ 5431.295972] usb 1-11: Manufacturer: SEGGER
    [ 5431.295972] usb 1-11: SerialNumber: 000682751220
    [ 5436.316606] cdc_acm 1-11:1.0: ttyACM0: USB ACM device
    ```

- Your serial device's path should be `/dev/tty____` based on what's shown on `dmesg`. For the example above, the path is `/dev/ttyACM0`

- Minicom requires some settings: run `sudo minicom -s` and follow the [getting started with Minicom setup](https://wiki.emacinc.com/wiki/Getting_Started_With_Minicom) with your serial device's path

- Then when you want to see your serial logs, run `sudo minicom` and you're good to go


### RTT logging:
There will likely be only 1 UART port on the custom board and we will probably need it for other important purposes (communicating with sensors, etc.) We can make use of the J-Link RTT viewer to display logs instead of using UART following [this tutorial](https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.8.0/nrf/gs_testing.html#testing-rtt)
