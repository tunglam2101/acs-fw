/* Reference from
 * https://devzone.nordicsemi.com/f/nordic-q-a/54099/how-can-i-send-data-from-nrf9160-to-external-module-using-uart
 * and
 * http://co2meters.com/Documentation/Other/AN_SE_0119_0177_AN168_Revised8.pdf
 */

#include "s8lp_manager.h"

#include <assert.h>
#include <logging/log.h>
#include <sys/__assert.h>

#define UART_BUF_SIZE 7

#define LOG_MODULE_NAME s8lp_manager
LOG_MODULE_REGISTER(LOG_MODULE_NAME);

static const s8lp_command_info_t k_s8lp_command = { /* CO2_READ */
	.m_co2_ret_high_idx = 3,
	.m_co2_ret_low_idx = 4,
	.m_command_bytes = { 0xFE, 0x04, 0x00, 0x03, 0x00, 0x01, 0xD5, 0xC5 }
};
static const uint16_t k_readings_delay_ms = 40;
static const char* k_device_name = "UART_0";

typedef struct uart_data_t
{
	void* fifo_reserved;
	uint8_t data[COMMAND_NUM_BYTES];
	uint16_t len;
} uart_data_t;

static struct device* p_uart_dev = NULL;
static uint8_t uart_rx_buf[UART_BUF_SIZE];
static uint8_t data_buf[UART_BUF_SIZE];
static uint8_t data_buf_curr_idx = 0;

static bool b_initialized = false;

static K_FIFO_DEFINE(fifo_uart_tx_data);
static K_FIFO_DEFINE(fifo_uart_rx_data);

void
sample_fetch_s8lp()
{
	uart_fifo_fill(p_uart_dev, (const uint8_t*)&k_s8lp_command.m_command_bytes,
		COMMAND_NUM_BYTES);
	uart_irq_tx_enable(p_uart_dev);
}

s8lp_readings_t
get_s8lp_readings()
{
	s8lp_readings_t ret = { .m_co2_readings = 0 };

	if (b_initialized)
	{
		sample_fetch_s8lp();
		k_sleep(K_MSEC(k_readings_delay_ms));

		uint8_t hi = data_buf[k_s8lp_command.m_co2_ret_high_idx];
		uint8_t lo = data_buf[k_s8lp_command.m_co2_ret_low_idx];
		uint32_t val = (hi << 8) + lo;
		ret.m_co2_readings = val;
	}

	return ret;
}

void
uart_cb(const struct device* dev, void* user_data)
{
	ARG_UNUSED(user_data);
	uart_irq_update(dev);

	int data_length = 0;
	if (uart_irq_rx_ready(dev))
	{
		data_length = uart_fifo_read(dev, uart_rx_buf, sizeof(uart_rx_buf));
		assert(data_length == 1);
		for (int i = 0; i < data_length; i++)
		{
			uint8_t data_buf_insert_idx = data_buf_curr_idx + i;
			data_buf[data_buf_insert_idx] = uart_rx_buf[i];
		}
	}
	data_buf_curr_idx += data_length;
	if (data_buf_curr_idx == UART_BUF_SIZE)
	{
		data_buf_curr_idx = 0;
	}

	if (uart_irq_tx_ready(dev))
	{
		uart_data_t* p_tx_buf = k_fifo_get(&fifo_uart_tx_data, K_NO_WAIT);

		if (!p_tx_buf)
		{
			uart_irq_tx_disable(dev);
			return;
		}

		uint32_t written = 0;
		while (p_tx_buf->len > written)
		{
			written += uart_fifo_fill(
				dev, &p_tx_buf->data[written], p_tx_buf->len - written);
		}

		while (!uart_irq_tx_complete(dev))
		{
			/* Wait for TX to complete */
		}

		if (k_fifo_is_empty(&fifo_uart_tx_data))
		{
			uart_irq_tx_disable(dev);
		}

		k_free(p_tx_buf);
	}
}

bool
init_s8lp()
{
	p_uart_dev = (struct device*)device_get_binding(k_device_name);
	if (!p_uart_dev)
	{
		LOG_ERR("Could not get UART device binding");
		return false;
	}

	uart_irq_rx_enable(p_uart_dev);
	uart_irq_callback_set(p_uart_dev, uart_cb);

	LOG_INF("S8 LP sensor initialized");

	b_initialized = true;
	return true;
}
