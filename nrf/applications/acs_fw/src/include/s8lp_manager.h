#pragma once

#include <stddef.h>
#include <stdint.h>

#include <drivers/uart.h>
#include <zephyr.h>
#include <zephyr/types.h>

#define COMMAND_NUM_BYTES 8

typedef struct s8lp_command_info_t
{
	int8_t m_co2_ret_high_idx;
	int8_t m_co2_ret_low_idx;
	uint8_t m_command_bytes[COMMAND_NUM_BYTES];
} s8lp_command_info_t;

typedef struct s8lp_readings_t
{
	uint32_t m_co2_readings;
} s8lp_readings_t;

/**
 * @brief Fetch a sample from the S8LP sensor
 */
void sample_fetch_s8lp();

/**
 * @brief Get the S8 LP readings object
 *
 * @return S8 LP readings object that contains the CO2 readings (0 and if such
 * reading is not included in the returned value of the input command)
 */
s8lp_readings_t get_s8lp_readings();

/**
 * @brief UART IRQ callback function
 *
 * @param dev UART device structure.
 * @param user_data Pointer to user's data (unused)
 */
void uart_cb(const struct device* dev, void* user_data);

/**
 * @brief Init sequence for S8 LP sensor
 *
 * @return True if S8LP is initialized successfully, false otw
 */
bool init_s8lp();
