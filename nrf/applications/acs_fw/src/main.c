/* main.c - Application main entry point */

/* Reference:
 * https://github.com/fduignan/zephyr_bbc_microbit_v2/tree/main/ble_minimal
 */

/* The FW deploys 3 services:
 * 1. 0x1800 - Generic ACCESS (GAP)
 * 2. 0x1801 - Generic Attribute (GATT - this is part of the software device
 * and is not used nor is it apparently removable see
 * https://devzone.nordicsemi.com/f/nordic-q-a/15076/removing-the-generic-attribute-0x1801-primary-service-if-the-service-changed-characteristic-is-not-present
 * 3. 0x181A - Environmental Sensing with 3 characteristics:
 * - 0x2A6E - Temperature
 * - 0x2A6F - Humidity
 * - 0x2BD5 - Particulate Matter - PM1 Concentration (TBD)
 *
 * More UUID at:
 * https://specificationrefs.bluetooth.com/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
 */

#include "battery_manager.h"
#include "bt_manager.h"
#include "s8lp_manager.h"
#include "sht3xd_manager.h"
#include "utils.h"
#ifdef CONFIG_DEBUG
#include <timing/timing.h>
#endif /* CONFIG_DEBUG */
#include <sys/reboot.h>
#include <task_wdt/task_wdt.h>

#define NUM_INITIALIZATION 3

#define LOG_MODULE_NAME main
LOG_MODULE_REGISTER(LOG_MODULE_NAME);

/* TODO: Tune these value later on to match with client's requirements */
#ifdef CONFIG_DEBUG
static const uint32_t k_default_main_loop_sleep_interval_ms = 4500;
static int64_t time_stamp = 0;
static uint32_t time_spent_ms = 0;
#else
static const uint32_t k_default_main_loop_sleep_interval_ms = 10000;
#endif /* CONFIG_DEBUG */
static const uint32_t k_default_watchdog_timeout_ms = 15000;
static const uint32_t k_default_small_sleep_interval_ms = 500;

/**
 * @brief Watchdog callback. Referenced from
 * zephyr/samples/subsys/task_wdt/src/main.c
 *
 * @param channel_id - Channel ID of task
 * @param user_data - User's data
 */
static void
task_wdt_callback(int channel_id, void* user_data)
{
	if (!is_connection_established())
	{
		task_wdt_feed(channel_id);
		return;
	}

	LOG_INF("Task watchdog channel %d callback", channel_id);

	/*
	 * If the issue could be resolved, call task_wdt_feed(channel_id) here
	 * to continue operation.
	 *
	 * Otherwise we can perform some cleanup and reset the device.
	 */

	LOG_INF("Resetting device...");

	sys_reboot(SYS_REBOOT_COLD);
}

void
main(void)
{
	/* Setup init sequence for BLE and sensors */
	device_status_t dev_status = FAIL;
	if (init_bt_manager())
	{
		dev_status |= BT_OK;
	}
	if (init_sht3xd())
	{
		dev_status |= SHT3XD_OK;
	}
	if (init_s8lp())
	{
		dev_status |= S8LP_OK;
	}
	if (!battery_measure_enable(true))
	{
		dev_status |= BATTERY_OK;
	}

	/* Init & start a watchdog */
	task_wdt_init(NULL); /* Init with a NULL ptr here because we're using a
							software watchdog */
	int task_wdt_id = task_wdt_add(k_default_watchdog_timeout_ms,
		task_wdt_callback, (void*)k_current_get());

	LOG_DBG("Finished initializting - Status: %s",
		log_strdup(device_status_to_string(dev_status)));
	/* Main loop */
	while (true)
	{
		while (!is_connection_established())
		{
			k_sleep(K_MSEC(k_default_small_sleep_interval_ms));
		}

#ifdef CONFIG_DEBUG
		time_stamp = k_uptime_get();
#endif /* CONFIG_DEBUG */
		/* Get sensors readings */
		sht3xd_readings_t sht3xd_readings = get_sht3xd_readings();
		s8lp_readings_t s8lp_readings = get_s8lp_readings();
		battery_readings_t battery_readings = battery_sample();

		LOG_DBG("Temperature: %d; Humidity: %d; CO2: %d; Battery: %d",
			sensor_value_to_int32(sht3xd_readings.m_temp),
			sensor_value_to_int32(sht3xd_readings.m_humid),
			s8lp_readings.m_co2_readings, battery_readings.m_battery_readings);

		/* Notify client */
		update_notify_bt_client(
			sht3xd_readings, s8lp_readings, battery_readings, dev_status);
#ifdef CONFIG_DEBUG
		time_spent_ms = (uint32_t)k_uptime_delta(&time_stamp);
		LOG_DBG("Total duration after BT notify: %dms", time_spent_ms);
#endif /* CONFIG_DEBUG */
		LOG_DBG("----------------------------------");
		/* Keep the watchdog fed */
		task_wdt_feed(task_wdt_id);

		/* Break down the main loop sleep into smaller 'chunks' of sleep to
		 * prevent the case where the client disconnects before
		 * k_default_main_loop_sleep_interval_ms ends */
		for (uint8_t i = 0; i < (k_default_main_loop_sleep_interval_ms
								 / k_default_small_sleep_interval_ms);
			 i++)
		{
			if (!is_connection_established())
			{
				break;
			}
			k_sleep(K_MSEC(k_default_small_sleep_interval_ms));
		}
	}
}
