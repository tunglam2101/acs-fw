#pragma once

#include "battery_manager.h"
#include "s8lp_manager.h"
#include "sht3xd_manager.h"
#include "utils.h"

#include <errno.h>
#include <logging/log.h>
#include <stddef.h>
#include <string.h>
#include <sys/byteorder.h>
#include <sys/printk.h>
#include <zephyr.h>
#include <zephyr/types.h>

#include <settings/settings.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/gatt.h>
#include <bluetooth/hci.h>
#include <bluetooth/uuid.h>
#include <device.h>
#include <drivers/sensor.h>
#include <stdio.h>

typedef enum chrc_offset_t
{
	TEMP_OFFSET = 1,
	HUMID_OFFSET = 2,
	CO2_OFFSET = 3,
	BATTERY_OFFSET = 4,
	DEV_STATUS_OFFSET = 5,
	/* Add more if needed */
} chrc_offset_t;

typedef struct gatt_chrc_t
{
	int32_t m_data;
	struct bt_uuid_16 m_uuid;
} gatt_chrc_t;
static const ssize_t k_default_chrc_data_size = sizeof(int32_t);

/**
 * @brief Callback for CCCD value change
 *
 * @param attr GATT attribute
 * @param value CCCD value to change
 */
void cccd_cfg_changed(const struct bt_gatt_attr* attr, uint16_t value);

/**
 * @brief Check if a BT connection has been established
 *
 * @return True if a connection has been established, false otw
 */
bool is_connection_established();

/**
 * @brief Callback that is activated when the characteristic is read by client
 *
 * @param conn   The connection that is requesting to read
 * @param attr   The attribute that's being read
 * @param buf    Buffer to place the read result in
 * @param len    Length of data to read
 * @param offset Offset to start reading from
 *
 * @return Number fo bytes read, or in case of an error
 *         BT_GATT_ERR() with a specific ATT error code.
 */
ssize_t read_chrc(struct bt_conn* conn,
	const struct bt_gatt_attr* attr,
	void* buf,
	uint16_t len,
	uint16_t offset);

/**
 * @brief Callback that is activated when a connection is established
 *
 * @param conn New connection object.
 * @param err HCI error. Zero for success, non-zero otherwise.otw
 *
 * @note: **IMPORTANT** If CONFIG_SECURITY_ENABLED=y, the function will try to
 * set the security level to BT_SECURITY_L4 and initiate a bonding sequence
 * after that. However, the maximum number of bonds that can be initiated is
 * CONFIG_BT_MAX_PAIRED. Any attempt to pair/bond more than
 * CONFIG_BT_MAX_PAIRED devices will crash the device --> It is best to define
 * a button that can allow us to unbond all previous cconnections before
 * starting a new one
 *
 * @p err can mean either of the following:
 * - @ref BT_HCI_ERR_UNKNOWN_CONN_ID Creating the connection started by
 *   @ref bt_conn_le_create was canceled either by the user through
 *   @ref bt_conn_disconnect or by the timeout in the host through
 *   @ref bt_conn_le_create_param timeout parameter, which defaults to
 *   @kconfig{CONFIG_BT_CREATE_CONN_TIMEOUT} seconds.
 * - @p BT_HCI_ERR_ADV_TIMEOUT High duty cycle directed connectable
 *   advertiser started by @ref bt_le_adv_start failed to be connected
 *   within the timeout.
 * - More in zephyr/include/bluetooth/hci_err.h
 */
void connected(struct bt_conn* conn, uint8_t err);

/** @brief Exchange MTU callback
 *
 *  @param conn Connection object.
 *  @param err Error code
 *  @param params Exchange MTU parameters.
 */
void exchange_func(struct bt_conn* conn,
	uint8_t err,
	struct bt_gatt_exchange_params* params);

/**
 * @brief Callback that is activated when a connection is disconnected
 *
 * @param conn Connection object.
 * @param reason HCI reason for the disconnection.
 */
void disconnected(struct bt_conn* conn, uint8_t reason);

#ifdef CONFIG_SECURITY_ENABLED
/** @brief Cancel ongoing authenticated pairing.
 *
 *  This function allows to cancel ongoing authenticated pairing.
 *
 *  @param conn Connection object.
 *
 *  @return Zero on success or negative error code otherwise
 */
void auth_cancel(struct bt_conn* conn);

/** @brief Reply if incoming pairing was confirmed by user.
 *
 *  This function should be called only after pairing_confirm callback from
 *  bt_conn_auth_cb structure was called if user confirmed incoming pairing.
 *
 *  @param conn Connection object.
 *
 *  @return Zero on success or negative error code otherwise
 */
void pairing_confirm(struct bt_conn* conn);

/** @brief notify that pairing procedure was complete.
 *
 *  This callback notifies the application that the pairing procedure
 *  has been completed.
 *
 *  @param conn Connection object.
 *  @param bonded Bond information has been distributed during the
 *                pairing procedure.
 */
void pairing_complete(struct bt_conn* conn, bool bonded);

/** @brief notify that pairing process has failed.
 *
 *  @param conn Connection object.
 *  @param reason Pairing failed reason
 */
void pairing_failed(struct bt_conn* conn, enum bt_security_err reason);

/** @brief The security level of a connection has changed.
 *
 *  This callback notifies the application that the security of a
 *  connection has changed.
 *
 *  The security level of the connection can either have been increased
 *  or remain unchanged. An increased security level means that the
 *  pairing procedure has been performed or the bond information from
 *  a previous connection has been applied. If the security level
 *  remains unchanged this means that the encryption key has been
 *  refreshed for the connection.
 *
 *  @param conn Connection object.
 *  @param level New security level of the connection.
 *  @param err Security error. Zero for success, non-zero otherwise.
 */
void security_changed(struct bt_conn* conn,
	bt_security_t level,
	enum bt_security_err err);

/** @brief Display a passkey to the user.
 *
 *  When called the application is expected to display the given
 *  passkey to the user, with the expectation that the passkey will
 *  then be entered on the peer device. The passkey will be in the
 *  range of 0 - 999999, and is expected to be padded with zeroes so
 *  that six digits are always shown. E.g. the value 37 should be
 *  shown as 000037.
 *
 *  This callback may be set to NULL, which means that the local
 *  device lacks the ability do display a passkey. If set
 *  to non-NULL the cancel callback must also be provided, since
 *  this is the only way the application can find out that it should
 *  stop displaying the passkey.
 *
 *  @param conn Connection where pairing is currently active.
 *  @param passkey Passkey to show to the user.
 */
void auth_passkey_display(struct bt_conn* conn, unsigned int passkey);

/** @brief Request the user to confirm a passkey.
 *
 *  When called the user is expected to confirm that the given
 *  passkey is also shown on the peer device.. The passkey will
 *  be in the range of 0 - 999999, and should be zero-padded to
 *  always be six digits (e.g. 37 would be shown as 000037).
 *
 *  Once the user has confirmed the passkey to match, the
 *  bt_conn_auth_passkey_confirm() API should be called. If the
 *  user concluded that the passkey doesn't match the
 *  bt_conn_auth_cancel() API should be called.
 *
 *  This callback may be set to NULL, which means that the local
 *  device lacks the ability to confirm a passkey. If set to non-NULL
 *  the cancel callback must also be provided, since this is the
 *  only way the application can find out that it should stop
 *  requesting the user to confirm a passkey.
 *
 *  @param conn Connection where pairing is currently active.
 *  @param passkey Passkey to be confirmed.
 */
void auth_passkey_confirm(struct bt_conn* conn, unsigned int passkey);
#endif /* CONFIG_SECURITY_ENABLED */

/**
 * @brief Advertise the BLE services and characteristics
 *
 * @return True if BT ready is successful, false otw
 */
bool bt_ready();

/**
 * @brief Init sequence for BT manager
 *
 * @return True if BT manager is initialized successfully, false otw
 *
 */
bool init_bt_manager();

/**
 * @brief Update the sensors readings and notify the connected BT client
 *
 * @param sht3xd_readings Temperature & humidity readings from SHT3x sensor
 * @param s8lp_readings CO2 readings from S8 LP sensor
 * @paran battery_readings Battery readings from ADC
 * @param dev_status Device's status after initialization
 */
void update_notify_bt_client(sht3xd_readings_t sht3xd_readings,
	s8lp_readings_t s8lp_readings,
	battery_readings_t battery_readings,
	device_status_t dev_status);
