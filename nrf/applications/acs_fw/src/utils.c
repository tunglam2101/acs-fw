#include "utils.h"

#include <logging/log.h>
#include <stdio.h>
#include <stdlib.h>
#include <zephyr.h>

static const uint8_t k_default_raw_precision_dp = 2;
static const uint8_t k_base = 10;

#define LOG_MODULE_NAME utils
LOG_MODULE_REGISTER(LOG_MODULE_NAME);

int32_t
sensor_value_to_int32(struct sensor_value val)
{
	int32_t int_part = val.val1;
	int32_t frac_part = val.val2;

	for (int i = 0; i < k_default_raw_precision_dp; i++)
	{
		int_part *= k_base;
	}

	char* frac_part_str = k_malloc(k_default_raw_precision_dp + 1);
	snprintf(frac_part_str, k_default_raw_precision_dp + 1, "%d", frac_part);
	frac_part = strtol(frac_part_str, NULL /*__endptr__*/, k_base);
	k_free(frac_part_str);

	return int_part + frac_part;
}

const char*
device_status_to_string(device_status_t dev_status)
{
	switch (dev_status)
	{
	case FAIL:
		break;
	case BT_OK:
		return "BT OK";
	case SHT3XD_OK:
		return "SHT3XD OK";
	case BT_SHT3XD_OK:
		return "BT & SHT3XD OK";
	case S8LP_OK:
		return "S8LP OK";
	case BT_S8LP_OK:
		return "BT & S8LP OK";
	case SHT3XD_S8LP_OK:
		return "SHT3XD & S8LP OK";
	case BT_SHT3XD_S8LP_OK:
		return "BT & SHT3XD & S8LP OK";
	case BATTERY_OK:
		return "BATTERY OK";
	case BT_BATTERY_OK:
		return "BT & BATTERY OK";
	case SHT3XD_BATTERY_OK:
		return "SHT3XD & BATTERY OK";
	case BT_SHT3XD_BATTERY_OK:
		return "BT & SHT3XD & BATTERY OK";
	case S8LP_BATTERY_OK:
		return "S8LP & BATTERY OK";
	case BT_S8LP_BATTERY_OK:
		return "BT & S8LP & BATTERY OK";
	case SHT3XD_S8LP_BATTERY_OK:
		return "SHT3XD & S8LP & BATTERY OK";
	case BT_SHT3XD_S8LP_BATTERY_OK:
		return "BT & SHT3XD & S8LP & BATTERY OK";
	default:
		break;
	}

	return "FAIL";
}
