#pragma once

#include "sht3xd_manager.h"

typedef enum device_status_t
{
	FAIL = 0,
	BT_OK = 1,
	SHT3XD_OK = 2,
	BT_SHT3XD_OK = 3,
	S8LP_OK = 4,
	BT_S8LP_OK = 5,
	SHT3XD_S8LP_OK = 6,
	BT_SHT3XD_S8LP_OK = 7,
	BATTERY_OK = 8,
	BT_BATTERY_OK = 9,
	SHT3XD_BATTERY_OK = 10,
	BT_SHT3XD_BATTERY_OK = 11,
	S8LP_BATTERY_OK = 12,
	BT_S8LP_BATTERY_OK = 13,
	SHT3XD_S8LP_BATTERY_OK = 14,
	BT_SHT3XD_S8LP_BATTERY_OK = 15,
} device_status_t;

/**
 * @brief Convert sensor_value to int32
 *
 * @note:
 * - sensor_value contains val1 as the integer part, val2 as the fractional
 * part
 * - Fractional part has a precision of 6 decimal places
 *
 * @param val Sensor value to convert
 * @return int32_t representation of sensor value. Note that this is generated
 * based on how many precision decimal places we want to render
 * (k_default_render_precision_dp)
 */
int32_t sensor_value_to_int32(struct sensor_value val);

/**
 * @brief Convert device's status flags to human-readable strings
 *
 * @return const char* Device's status as a human-readable string
 */
const char* device_status_to_string(device_status_t);
