#!/usr/bin/env bash

set -e

CWD=$(pwd)

KITWARE_ARCHIVE="kitware-archive.sh"
GN_ZIP="gn.zip"
GN_BIN="gn"
TOOLCHAIN_VER="9_2019-q4-major"
TOOLS_DIR="${HOME}/acs-fw-tools"
TOOLCHAIN_FILENAME="gcc-arm-none-eabi-9-2019-q4-major"
TOOLCHAIN_TARBALL_SUFFIX="-x86_64-linux.tar.bz2"
NRF_CMD_TOOL_ZIP="nrf-command-line-tools-10.15.0_amd.zip"
NRF_CMD_TOOL="nrf-command-line-tools_10.15.0_amd64.deb"

SDK_REPO_URL="https://github.com/nrfconnect/sdk-nrf"
SDK_VER="v1.8.0"

GN_DIR="${TOOLS_DIR}/gn"
TOOLCHAIN_DIR="${TOOLS_DIR}/gnuarmemb"

# Pre-authorize sudo
sudo echo

mkdir -p ${TOOLS_DIR} && cd ${TOOLS_DIR}

# Download, inspect and execute the Kitware archive script to add the Kitware APT repository to your sources list
echo "**** Adding the Kitware APT repo to sources list and update packages ****"
cd ${TOOLS_DIR}
if [ ! -e ${KITWARE_ARCHIVE} ]; then
  wget "https://apt.kitware.com/${KITWARE_ARCHIVE}"
fi
sudo sh ./${KITWARE_ARCHIVE}
sudo apt install --no-install-recommends -y git cmake ninja-build gperf \
  ccache dfu-util device-tree-compiler wget \
  python3-dev python3-pip python3-setuptools python3-tk python3-wheel xz-utils file \
  make gcc gcc-multilib g++-multilib libsdl2-dev
rm ./${KITWARE_ARCHIVE}

# Install the GN tool
echo "**** Installing the GN tool ****"
if [ ! -e ${GN_BIN} ]; then
  mkdir -p ${GN_DIR} && cd ${GN_DIR}
  if [ ! -e ${GN_ZIP} ]; then
    wget -O ${GN_ZIP} "https://chrome-infra-packages.appspot.com/dl/gn/gn/linux-amd64/+/latest"
  fi
  unzip -o ./${GN_ZIP}
  rm ./${GN_ZIP}
  echo "export PATH=${GN_DIR}:\$PATH" >> ${HOME}/.bashrc
  source ${HOME}/.bashrc
fi

# Install GNUARMENB toolchain
echo "**** Installing GNUARMENB toolchain ****"
if [ ! -z "$(ls -A ${TOOLCHAIN_DIR})"]; then
  mkdir -p ${TOOLCHAIN_DIR} && cd ${TOOLCHAIN_DIR}
  if [ ! -e ${TOOLCHAIN_FILENAME}${TOOLCHAIN_TARBALL_SUFFIX} ]; then
    wget -O ${TOOLCHAIN_FILENAME}${TOOLCHAIN_TARBALL_SUFFIX} "https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/${TOOLCHAIN_FILENAME}${TOOLCHAIN_TARBALL_SUFFIX}?revision=108bd959-44bd-4619-9c19-26187abf5225&hash=8B0FA405733ED93B97BAD0D2C3D3F62A"
  fi
  tar -xf ${TOOLCHAIN_FILENAME}${TOOLCHAIN_TARBALL_SUFFIX}
  mv ${TOOLCHAIN_FILENAME} ${TOOLCHAIN_VER}
  echo 'export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb' >> ${HOME}/.bashrc
  echo "export GNUARMEMB_TOOLCHAIN_PATH=${TOOLCHAIN_DIR}/${TOOLCHAIN_VER}" >> ${HOME}/.bashrc
  source ${HOME}/.bashrc
  rm ./${TOOLCHAIN_FILENAME}${TOOLCHAIN_TARBALL_SUFFIX}
fi

# Install nRF Command Line Tools
echo "**** Installing nRF Command Line Tools ****"
cd ${TOOLS_DIR}
if [ ! -e ${NRF_CMD_TOOL_ZIP} ]; then
  wget https://www.nordicsemi.com/-/media/Software-and-other-downloads/Desktop-software/nRF-command-line-tools/sw/Versions-10-x-x/10-15-0/nrf-command-line-tools-10.15.0_amd.zip
fi
unzip -j ${NRF_CMD_TOOL_ZIP} ${NRF_CMD_TOOL}
sudo dpkg -i ${NRF_CMD_TOOL}
rm ./nrf-command-line-tools*

# Install additional Python dependencies
cd ${CWD}
echo "**** Installing additional Python dependencies ****"
pip3 install --user -r zephyr/requirements.txt
pip3 install --user -r nrf/requirements.txt
pip3 install --user -r bootloader/requirements.txt
# For running `west` commands
echo 'export PATH=~/.local/bin:$PATH' >> ${HOME}/.bashrc
source ${HOME}/.bashrc

# West init & update
cd ..
pip3 install --user west
west init -m ${SDK_REPO_URL} --mr ${SDK_VER}
west update
west zephyr-export

echo "**** We're good to goooooo (⌐■_■) ****"

