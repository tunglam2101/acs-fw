/* Reference from Zephyr's sample zephyr/samples/sensor/sht3xd */

#include "sht3xd_manager.h"

#include <device.h>
#include <logging/log.h>
#include <stdio.h>
#include <zephyr.h>

#define LOG_MODULE_NAME sht3xd_manager
LOG_MODULE_REGISTER(LOG_MODULE_NAME);

static struct device* p_device = NULL;
static const char* k_device_name = "SHT3XD";

static bool b_initialized = false;

bool
init_sht3xd()
{
	p_device = (struct device*)device_get_binding(k_device_name);

	if (p_device == NULL)
	{
		LOG_ERR("Could not get SHT3XD device");
		return false;
	}

	LOG_INF("SHT3x sensor initialized");

	b_initialized = true;
	return true;
}

sht3xd_readings_t
get_sht3xd_readings()
{
	/* Based on the samples in zephyr/include/drivers/sensor.h,
	 * sensor_value.val2 always has 6 digits (i.e. sensor value has a precision
	 * of 6 decimal places) */
	struct sht3xd_readings_t ret = { 0 };

	if (b_initialized)
	{
		int err = sensor_sample_fetch(p_device);
		if (err == 0)
		{
			err = sensor_channel_get(
				p_device, SENSOR_CHAN_AMBIENT_TEMP, &ret.m_temp);
		}
		if (err == 0)
		{
			err = sensor_channel_get(
				p_device, SENSOR_CHAN_HUMIDITY, &ret.m_humid);
		}
		if (err != 0)
		{
			LOG_ERR("SHT3XD: failed: %d", err);
		}
	}

	return ret;
}
